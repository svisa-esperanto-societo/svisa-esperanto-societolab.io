## Esperanto-Kurse für Deutschsprachige:

- "Hallo Welt!" Esperanto-Radio-Kurs für Deutschsprachige auf Kanal8610\
Infos und Links: <https://informejo.com/esperanto-en-radio-kanal8610.html>


- Esperanto-Online lernen mit book2 (Goethe-Verlag)\
Link: <https://www.goethe-verlag.com/deutsch-esperanto-lernen-online.html>

- Eperanto-Fernkurs (Esperanto in Deutschland)\
Link: https://www.esperanto.de/de/fernkurs
