**{{ .nomo }}**

{{- if isset . "rolo" -}}
, _{{ .rolo }}_
{{- end -}}

{{- if isset . "adreso" -}}
, {{ .adreso -}}
{{- end -}}

{{- if isset . "telefonnumeroj" -}}
, {{ T "telefonnumero" (len .telefonnumeroj) }}: {{ delimit .telefonnumeroj ", " (T "kaj") -}}
{{- end -}}

{{- if isset . "retadreso" -}}
, {{ T "retadreso" }}:&nbsp;{{ .retadreso -}}
{{- end -}}
