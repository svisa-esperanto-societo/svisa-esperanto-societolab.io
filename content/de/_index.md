+++
title = "Willkommen"

description = "Die Schweizerische Esperanto-Gesellschaft (SES) verbindet Menschen, die Esperanto sprechen!"

[cascade]
featured_image = 'images/2016-07-29 Filmu (Foto Dietrich Michael Weidmann) blurred.jpg'
+++

Die SES wurde 1903 gegründet
und
ist der Landesverband des [Esperanto-Weltbundes (Universala Esperanto-Asocio)](https://uea.org/).
SES publiziert
das Bulletin "{{< verko "SES informas" >}}", das
den Mitgliedern in gedruckter Version zugestellt wird
und auch
über unsere Website online lesbar ist,
sowie
die viersprachige Zeitschrift
[{{< verko "Svisa Espero" >}} - Zeitschrift für Nachhaltigkeit und Sprachpolitik](https://esperanto.ch/svisa-espero/)
auf Deutsch, Französisch, Italienisch und Esperanto.
Die SES
organisiert und unterstützt nationale und lokale Treffen,
subventioniert Aktivitäten der lokalen Esperanto-Gruppen und Mitglieder,
organisiert Esperanto-Kurse,
verlegt Esperanto-Bücher
und
publiziert Informationsmaterial in und über Esperanto.
