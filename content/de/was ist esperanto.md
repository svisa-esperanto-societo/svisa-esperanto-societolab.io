+++
title = "Was ist Esperanto"
translationKey = 'kio estas Esperanto'
aliases = [
    "/de/esperanto",
    "/de/was-ist-esperanto.html",
]
+++

Esperanto ist eine Plansprache,
die mit dem Ziel geschaffen wurde,
dereinst weltweit als internationale Brückensprache
und Verständigungsmittel
zwischen allen Menschen verschiedener Muttersprache
zu dienen.
Das erste Esperanto-Lehrbuch wurde
1887
vom polnischen Augenarzt Ludoviko Lazara Zamenhof
publiziert.
Seither hat sich Esperanto
trotz grosser Vorurteile und Widerstände
langsam aber stetig
immer weiter verbreitet
und
immer mehr Anhänger gefunden.
Heute sprechen
mehrere Millionen Menschen
in allen Ländern der Welt
Esperanto.
Regelmässig finden
überall auf der Welt
zahlreiche Treffen und Kongresse statt,
der bedeutendste davon ist der Esperanto-Weltkongress.

[Einen Überblick über die Esperanto-Grammatik gibt es hier][mallonge]!

Um
mehr über Esperanto zu erfahren
und
die Sprache online zu lernen
empfehlen wir
die Webplattform [lernu.net](https://lernu.net/).

---

{{% eo-kursoj_de %}}

{{% internaciaj_eo-kursoj %}}

---

## Esperanto auf einen Blick

[Infoblatt in deutscher Sprache][mallonge]

[mallonge]: https://esperanto.ch/kurso/Esperanto-Mallonge-DE.pdf
