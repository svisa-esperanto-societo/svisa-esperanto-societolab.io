+++
title = "Qu'est-ce que l'espéranto?"
translationKey = 'kio estas Esperanto'
slug = "qu-est-ce-que-l-esperanto"
aliases = [
    "/fr/esperanto",
    "/fr/qu-est-ce-que-l-esperanto-.html",
]
+++

L’Espéranto a été proposé
comme langue internationale neutre
en 1887
par le docteur L.L. Zamenhof,
un médecin
qui vivait
dans la partie occidentale de l’empire russe.
Il a créé une langue facile à apprendre et régulière,
qui pourrait ensuite évoluer
et
devenir une seconde langue internationale pour tous.
Zamenhof considérait qu’une langue neutre,
appartenant à tous les hommes,
constituerait non seulement un apport pratique,
mais contribuerait aussi
à l’atténuation des conflits
et
à la promotion de la paix.

L’Espéranto a déjà atteint un résultat majeur :
il est devenu une langue vivante transgénérationnelle,
et est utilisé par des milliers de locuteurs dans le monde.
Certains se regroupent au sein d’associations.
L’Association Universelle d’Espéranto,
UEA (Universala Esperanto-Asocio),
enregistre des membres dans plus de 125 pays.
L’Association Mondiale Anationale,
SAT (Sennacieca Asocio Tutmonda),
d’ampleur plus réduite,
compte près d’un millier de membres.
Elle constitue un lieu de rencontres
pour
des espérantistes de gauche,
des syndicalistes
et
des écologistes.
ILEI,
la Ligue Internationale des Enseignants Espérantistes,
compte également près d'un millier de membres.
Il existe
des associations nationales
et
55 associations spécialisées.
L’Espéranto prouve en permanence sa vitalité.
Il rend aussi de nombreux services pratiques à ses locuteurs.

[Apérçu de la grammaire][mallonge]!

Pour
en savoir plus sur l'espéranto
et
apprendre la langue en ligne,
nous vous recommandons
la plateforme web [lernu.net](https://lernu.net/).

---

{{% eo-kursoj_fr %}}

{{% internaciaj_eo-kursoj %}}

---

## L'espéranto en un clin d'œil

[fiche d'information en français][mallonge]

[mallonge]: https://esperanto.ch/kurso/Esperanto-Mallonge-FR.pdf
