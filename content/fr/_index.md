+++
title = "Bienvenue"

description = "La Société Suisse d’Espéranto (SES) relie des hommes et des femmes qui parlent l'espéranto !"

[cascade]
featured_image = 'images/2016-07-29 Filmu (Foto Dietrich Michael Weidmann) blurred.jpg'
+++

SES a été fondée en 1903
et
constitue la section suisse de l’[Association Universelle d’Espéranto (Universala Esperanto-Asocio)](https://uea.org/).
SES édite
le bulletin « {{< verko "SES informas" >}} »,
envoyé sous forme imprimée aux membres de l’Association
et
lisible également en ligne sur notre site,
et
[{{< verko "Svisa Espero" >}} - Revue sur le développement durable et la politique linguistique](https://esperanto.ch/svisa-espero/)
en français, italien, allemand et espéranto.
SES
organise des rencontres nationales et locales,
soutient des activités de groupes locaux ou de membres,
organise des cours d’espéranto,
édite des livres
et
publie du matériel d’information en espéranto et à son sujet.
