+++
title = "Bonvenon"

description = "Svisa Esperanto-Societo (SES) interligas homojn, kiuj parolas Esperanton!"

[cascade]
featured_image = 'images/2016-07-29 Filmu (Foto Dietrich Michael Weidmann) blurred.jpg'
+++

SES estis fondita 1903
kaj
estas la landa sekcio de [Universala Esperanto-Asocio](https://uea.org/).
SES eldonas
la bultenon {{< verko "SES informas" >}}
sendata en papera formo al la membroj de la asocio
kaj ankaŭ
legebla rete en nia retejo
kaj
la kvarlingvan revuon
[{{< verko "Svisa Espero" >}} - Revuo pri daǔripovo kaj lingvopolitiko](https://esperanto.ch/svisa-espero/).
SES
organizas naciajn kaj lokajn renkontiĝojn,
subtenas aktivecojn de lokaj grupoj kaj membroj,
organizas Esperanto-kursojn,
kaj
eldonas librojn kaj informomaterialon en kaj pri Esperanto.
