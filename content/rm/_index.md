+++
title = "Bainvegni"

description = "La Societad svizra d'esperanto (SES) collia umans che discurran esperanto!"

[cascade]
featured_image = 'images/2016-07-29 Filmu (Foto Dietrich Michael Weidmann) blurred.jpg'
+++

La SES è vegnida fundada il 1903.
Ella è la societad svizra da l'[Associaziun universala d'esperanto (Universala Esperanto-Asocio)](https://uea.org/).
La SES publitgescha
il bulletin «{{< verko "SES informas" >}}»
– ch'ils commembers survegnan en furma stampada
e
ch'ins po leger online sin nossa pagina-web –
sco er
il magazin
[{{< verko "Svisa Espero" >}} – in magazin per durabilitad e politica linguistica](https://esperanto.ch/svisa-espero/)
en tudestg, franzos, talian ed esperanto.
La SES
organisescha e sustegna inscunters naziunals e locals,
subvenziunescha activitads da las gruppas e dals commembers d'esperanto locals,
organisescha curs d'esperanto,
edescha cudeschs en esperanto
e
publitgescha material d'infurmaziun en e davart esperanto.
