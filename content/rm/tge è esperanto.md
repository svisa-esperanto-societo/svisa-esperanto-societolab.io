+++
title = "Tge è esperanto?"
translationKey = 'kio estas Esperanto'
slug = "tge-e-esperanto"
aliases = [
    "/rm/esperanto",
    "/rm/tge-e-esperanto-.html",
]
+++

Esperanto è ina lingua planisada ch'è vegnida creada cun la finamira da servir in di en l'entir mund sco lingua da punt internaziunala e med da communicaziun tranter tut ils umans da differentas linguas maternas. L'emprim cudesch d'emprender esperanto ha il medi dad egls polac Ludoviko Lazara Zamenhof publitgà il 1887. Malgrà gronds pregiudizis e resistenza è esperanto dapi lura sa derasà plaunsieu, ma cuntinuadamain ed ha survegnì adina dapli aderents. Oz discurran plirs milliuns umans en tut ils pajais dal mund esperanto. Regularmain han lieu dapertut sin quest mund numerus inscunters e congress, il pli impurtant è il congress mundial d'esperanto.

[Qua datti ina survista da la grammatica d'esperanto!][mallonge]

Per vegnir a savair dapli davart l'esperanto
e
per emprender online la lingua
cussegliain nus
la plattafurma-web [lernu.net](https://lernu.net/).

---

<!-- Ni ne scias, ĉu ekzistas Eo-kursoj por rumanĉ-lingvanoj,
do ni listigas kursojn por german-lingvanoj. -->
{{% eo-kursoj_de %}}

{{% internaciaj_eo-kursoj %}}

---

## Esperanto en in'egliada

[fegl d'infurmazion en rumantsch][mallonge]

[mallonge]: https://esperanto.ch/kurso/Esperanto-Mallonge-RM.pdf
