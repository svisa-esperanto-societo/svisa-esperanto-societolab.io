+++
title = "Benvenuti"

description = "La Società Esperantista Svizzera (SES) unisce le persone che parlano esperanto!"

[cascade]
featured_image = 'images/2016-07-29 Filmu (Foto Dietrich Michael Weidmann) blurred.jpg'
+++

La SES fu fondata nel 1903
ed
è l’associazione nazionale dell’[Associazione Universale dell’Esperanto (Universala Esperanto-Asocio)](https://uea.org/).
La SES pubblica
il bollettino "{{< verko "SES informas" >}}",
che viene distribuito ai membri in forma stampata
e
che può essere letto anche tramite il nostro sito web,
e
la rivista in quattro lingue
[{{< verko "Svisa Espero" >}} Rivista sulla sostenibilità e la politica linguistica](https://esperanto.ch/svisa-espero/)
in tedesco, francese, italiano ed esperanto.
La SES
organizza e supporta incontri nazionali e locali,
sovvenziona attività dei gruppi esperantisti locali e dei loro membri,
organizza corsi di esperanto,
funge da editrice di libri in esperanto
e
pubblica materiale informativo in esperanto e sull’esperanto.
