+++
title = "Cos’è l’Esperanto?"
translationKey = 'kio estas Esperanto'
slug = "cos-e-l-esperanto"
aliases = [
    "/it/esperanto",
    "/it/cos-e-l-esperanto-.html",
]
+++

L’esperanto è una lingua pianificata
creata con l’obiettivo
di servire da lingua ponte
e
da mezzo di comunicazione
tra tutte le persone di diverse madrelingue.
Il primo libro per l’apprendimento dell’esperanto
fu pubblicato
nel 1887
dall’oculista polacco Ludoviko Lazara Zamenhof.
Nonostante i grandi pregiudizi e le resistenze,
da allora l’esperanto si
è lentamente ma costantemente diffuso sempre di più
e
ha trovato sempre più sostenitori.
Attualmente l’esperanto viene parlato
da diversi milioni di persone
in tutti i paesi del mondo.
In tutto il mondo vengono svolti regolarmente incontri e congressi.
Il più importante di essi è il Congresso mondiale dell’esperanto.

[Una panoramica della grammatica dell’esperanto!][mallonge]

Per ulteriori informazioni sull’Esperanto
e
per imparare la lingua online
consigliamo
la piattaforma web [lernu.net](https://lernu.net/).

---

{{% eo-kursoj_it %}}

{{% internaciaj_eo-kursoj %}}

---

## L'Esperanto in breve

[foglio informativo in italiano][mallonge]

[mallonge]: https://esperanto.ch/kurso/Esperanto-Mallonge-IT.pdf
